from flask import Blueprint
from flask_restx import Api

from app.webhook.dialogflow.routes import api as api_dialogflow


webhook_bp = Blueprint('webhook', __name__, url_prefix='/webhook')
api = Api(
    webhook_bp,
    version='0.1',
    description='Conversa - Sample eCommerce Webhook',
    doc='/doc/'
)

api.add_namespace(api_dialogflow, '/dialogflow')
