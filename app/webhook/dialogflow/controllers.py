class WebhookHandler:
    def __init__(self, webhook_request):
        # Full payload can be seen at https://cloud.google.com/dialogflow/es/docs/fulfillment-webhook#webhook_request
        self.webhook_request = webhook_request
        self.intent_display_name = self.webhook_request['queryResult']['intent']['displayName']

    def generate_response(self):
        if self.intent_display_name == '<your-intent-display-name>':
            # do something

            return {
                'fulfillmentMessages': [{
                    'text': {
                        'text': ['<your-dynamic-response>']
                    }
                }]
            }
