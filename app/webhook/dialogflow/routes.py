from flask import request, session
from flask_restx import Namespace, Resource

from app.webhook.dialogflow import controllers


api = Namespace('DialogflowWebhook', description='Manage webhook for Dialogflow')


@api.route('/')
class ReceiveWebhook(Resource):
    @api.doc('Receive data from Dialogflow')
    def post(self):
        # parse payload
        payload = request.json
        response = controllers.WebhookHandler(payload).generate_response()
        
        return response, 200
