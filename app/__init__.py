from flask import Flask
from app import webhook


app = Flask(__name__, instance_relative_config=True)
app.config.from_object('config')


@app.route('/', methods=['GET'])
def index():
    return 'Hello, world!'


from app.webhook import webhook_bp
app.register_blueprint(webhook_bp)

