from dotenv import load_dotenv

# for local deployment
load_dotenv()


# APP
APP_NAME = 'Sample Dialogflow Webhook'
DEBUG = True