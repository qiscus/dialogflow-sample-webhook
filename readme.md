# Sample Dialogflow Webhook

This is a sample code to create dynamic response in Dialogflow using webhook.

## Usage
```bash
python run.py
```
Open [localhost:5000](localhost:5000) to check whether app run successfully or not.

Dialogflow only accept `https`, you can try this feature through your local machine by using [ngrok](https://ngrok.com/).

## Qiscus Robolabs Setup
1. Navigate to Intent -> Webhook.
2. Enable webhook and insert your webhook service URL. Insert https://`<your-base-URL>`/webhook/dialogflow if you are using this sample. 

![webhook settings](https://bitbucket.org/qiscus/dialogflow-sample-webhook/raw/8e40035b8140775aadfbc37f750c4629e033f1bf/webhook-settings.png)

3. Enable webhook state on the desired intent.

![webhook intent settings](https://bitbucket.org/qiscus/dialogflow-sample-webhook/raw/8e40035b8140775aadfbc37f750c4629e033f1bf/webhook-intent-settings.png)